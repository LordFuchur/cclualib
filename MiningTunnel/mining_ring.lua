#!/usr/bin/lua5.1

-- import Mining Ring 
os.loadAPI('MiningTunnel/proxyturtle.lua')
-- set alias
PT = proxyturtle


MiningRing =
{
	-- Exceptions
  exception_args		= 0,
}

MiningRing.__index = MiningRing

---------------------------------------------------------------------------------

--- MiningRing class constructor
-- @param pTurtle An instance of a proxyturtle
-- @return Return the instance of the MiningRing object
function MiningRing:new(pTurtle)
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,MiningRing)
  -- create own proxyturtle when no given
  if(pTurtle == nil) then
    pTurtle = PT.ProxyTurtle:new()
  end
  -- set class variables
  instance.pTurtle = pTurtle
  -- return the new instance of the class
  return instance
end

---------------------------------------------------------------------------------

--- Print exeception
-- @param e Exeception message
function MiningRing:exception(e)
	if ( e == self.exception_args ) then
		print("Exception: Invalid or to few arguments")
  end
end

---------------------------------------------------------------------------------

--- Check direction and dig so long a block exist in given direction
-- @param direction string Takes a direction string: "up";"down";"forward"
function MiningRing:checkDig(direction)
  if(direction == "up") then
    while(self.pTurtle:detectUp()) do
      self.pTurtle:digUp()
    end
  elseif(direction == "down") then
    while(self.pTurtle:detectDown()) do
      self.pTurtle:digDown()
    end    
  elseif(direction == "forward") then
    while(self.pTurtle:detect()) do
      self.pTurtle:dig()
    end
  else  
    print("Invalid dig direction!")
  end
end

--- Dig one ring (tunnel layer) of given size
-- @param width The width of the ring.
-- @param height The height of the ring
-- @param startPos The starting position of the turtle.
-- This can be "left" or "right" as string
-- @param returnStartPos boolean Shall the turtle drive back ?
function MiningRing:DigRing(width,height,startPos,returnStartPos)
  -- check arguments
  if(startPos == nil or width < 1 or height < 1) then MiningRing:exception(self.exception_args) end
  -- local variables
  local side
  local isEven = width % 2
  -- dig right into the ring
  self:checkDig("forward")
  self.pTurtle:forward()
  side = true
  for x = 1, (width) do 
    for y = 1, (height-1) do
      if(side) then
        -- dig up
        self:checkDig("up")
        self.pTurtle:up()
        -- sand and gravel would be fall through the turtle and just drop to the ground
        self.pTurtle:suckUp()
      else
        -- dig down
        self:checkDig("down")
        self.pTurtle:down()
      end
    end
    -- When reach the last column dont move into the next column
    if(x ~= width) then
      if(startPos == "right") then
        -- go to next column
        self.pTurtle:turnLeft()
        self:checkDig("forward")
        self.pTurtle:forward()
        self.pTurtle:turnRight()
      elseif(startPos == "left") then
        -- go to next column
        self.pTurtle:turnRight()
        self:checkDig("forward")
        self.pTurtle:forward()
        self.pTurtle:turnLeft()      
      end
    else -- Move back into start position plus Z + 1
      if(isEven  ~= 0) then
        -- Move down
        for y = 1, (height-1) do
          self.pTurtle:down()
        end
      end
      -- skip driving back when needed
      if(returnStartPos) then
        return
      end
      if(startPos == "right") then
        -- drive back
        self.pTurtle:turnLeft()
        for x = 1, (width - 1) do 
          self.pTurtle:back()
        end
        self.pTurtle:turnRight()
      elseif(startPos == "left") then
        -- drive back
        self.pTurtle:turnRight()
        for x = 1, (width - 1) do 
          self.pTurtle:back()
        end
        self.pTurtle:turnLeft()        
      end
    end
    side = not side
  end -- end for X loop
end


---------------------------------------------------------------------------------

--[[ Example 1

-- create an instance of the MiningRing
local test = MiningRing:new()
-- do the wanted work
test:DigRing(6,5,"left")

--]]


--[[ Example 2

-- substitute function for turtle.forward
function testForward()
  if(turtle.detect()) then
    print("Something is in front of me!")
  else
    turtle.forward()
  end
end

-- create a proxyturtle
local pTurtle = PT.ProxyTurtle:new()
pTurtle:SetProxyFunction("pt_forward",testForward)

-- create an instance of the MiningRing
local test = MiningRing:new(pTurtle)
-- do the wanted work
test:DigRing(6,5,"left")

--]]
