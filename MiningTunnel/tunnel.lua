#!/usr/bin/lua5.1
-- import Mining Tunnel 
os.loadAPI('MiningTunnel/mining_tunnel.lua')
-- set alias
MT = mining_tunnel

---------------------------------------------------------------------------------------

function header()
  shell.run("clear")
  print("-----------------------------")
  print("------> Mining Tunnel <------")
  print("-----------------------------\n")
  print("Note: It will dig and move forward and start the tunnel from there.")
  print("From his position up to the height and\ndependening on the start position to the left or right.")
end

local function inputValue(message)
    local val = nil
    -- menu loop
    repeat
        print(message)
        val = io.read("*l")
        val = tonumber(val)
        assert(type(val) == "number")
    until( val ~= nil)
    return val
end

local function inputString(message)
    local str = nil
    -- menu loop
    repeat
        print(message)
        str = io.read()
        assert(type(str) == "string")
    until( str ~= "left" or str ~= "right")
    return str  
end

---------------------------------------------------------------------------------------

header()
local tunnel = MT.MiningTunnel:new()

local width = nil
local height = nil
local length = nil
local startPos = nil
width     = inputValue("\nPlease enter width: ")
height    = inputValue("\nPlease enter height: ")
length    = inputValue("\nPlease enter length: ")
startPos  = inputString("\nPlease enter start position (left/right): ")

if(width ~= nil and height ~= nil and length ~= nil and startPos ~= nil) then
  tunnel:DigTunnel(width,height,length,startPos)
else
  print("Something went wrong")
end
