#!/usr/bin/lua5.1
-- import Mining Ring 
os.loadAPI('MiningTunnel/mining_ring.lua')
-- set alias
MR = mining_ring

if (not MR) then
  print("Couldnt load mining_ring.lua")
  shell.exit()
end

MiningTunnel =
{
	-- Exceptions
  exception_args		= 0,
}

MiningTunnel.__index = MiningTunnel

---------------------------------------------------------------------------------

--- MiningTunnel class constructor
-- @param pTurtle An instance of a proxyturtle
-- @return Return the instance of the MiningTunnel object
function MiningTunnel:new(pTurtle)
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,MiningTunnel)
  -- set class variables
  if(pTurtle ~= nil) then
    instance.pTurtle = pTurtle
  else
    instance.pTurtle = nil
  end
  -- return the new instance of the class
  return instance
end

---------------------------------------------------------------------------------

--- Print exeception
-- @param e Exeception message
function MiningTunnel:exception(e)
	if ( e == self.exception_args ) then
		print("Exception: Invalid or to few arguments")
  end
end

--- Take a starting position and return the opposite starting position
-- @param startPos The starting position of the turtle.
-- @return string The opposite starting position of the turtle.
function MiningTunnel:GetOpposite(startPos)
  if(startPos == "left") then
    return "right"
  elseif(startPos == "right") then
    return "left"
  else
    print("Invalid starting position")
  end
end

---------------------------------------------------------------------------------

--- Dig a tunnel of given size
-- @param width The width of the tunnel.
-- @param height The height of the tunnel.
-- @param length The length of the tunnel.
-- @param startPos The starting position of the turtle.
-- This can be "left" or "right" as string
function MiningTunnel:DigTunnel(width,height,length,startPos)
  local currentStartPos = startPos
    -- check arguments
  if(startPos == nil or width < 1 or height < 1 or length < 1) then MiningTunnel:exception(self.exception_args) end
  -- local variables
  local miner = MR.MiningRing:new()
  -- setup proxyturtle
  if(self.pTurtle ~= nil) then
    miner:SetProxyTurtle(self.pTurtle)
  end
  -- dig the tunnel
  for z = 1, length do
    miner:DigRing(width,height,currentStartPos,true)
    currentStartPos = MiningTunnel:GetOpposite(currentStartPos)
  end
end

---------------------------------------------------------------------------------

--[[ Example 1

-- create an instance of the MiningTunnel
local tunnel = MiningTunnel:new()
-- do the wanted work
tunnel:DigTunnel(6,5,3,"right")

]]--

--[[ Example 2

-- substitute function for turtle.forward
function testForward()
  if(turtle.detect()) then
    print("Something is in front of me!")
  else
    turtle.forward()
  end
end

-- create a proxyturtle
local pTurtle = PT.ProxyTurtle:new()
pTurtle:SetProxyFunction("pt_forward",testForward)

-- create an instance of the MiningTunnel
local test = MiningTunnel:new(pTurtle)

-- do the wanted work
tunnel:DigTunnel(6,5,3,"right")
]]--

