#!/usr/bin/lua5.1
-- Connection defines
local SETTINGS = 
{
  user="LordFuchur", 
  page="gitlab.com",
  project="cclualib",
  debug=false,
  programs_per_page=4,
}
-- Program defines
local PROGRAMS = 
{
    {index=1, name="Blackbox", folder="Blackbox"},
    {index=2, name="Stationfarm", folder="Stationfarm"},
    {index=3, name="MiningTunnel", folder="MiningTunnel"},
    {index=4, name="Turtle Controller", folder="TurtleController"},
    {index=5, name="Utils", folder="Utils"},
    {index=6, name="Tree Farm", folder="Treefarm"},
}

---------------------------------------------------------------------

function header()
  shell.run("clear")
  print("------------------------------")
  print("---> Repository Installer <---")
  print("------------------------------\n")
end


function checkPageNumber(page)
  local div = (table.getn(PROGRAMS) / SETTINGS.programs_per_page)
  local limit = math.ceil(div)
  return (page <= limit)  
end

function buildURL()
    local TEMPLATE=[[https://{{page}}/{{user}}/{{project}}/]]
    local ret
    if(SETTINGS.page == "gitlab.com") then
      ret = TEMPLATE:gsub("{{(%w+)}}",SETTINGS)
      ret = ret .. "raw/master/"
    elseif(SETTINGS.page == "github.com") then
      SETTINGS.page = "raw.githubusercontent.com"
      ret = TEMPLATE:gsub("{{(%w+)}}",SETTINGS)
      ret = ret .. "master/"
    else
      print("ERROR: Unknown page\n")
      return nil
    end
    return ret
end

function showpage(page)
  local TEMPLATE = [[{{index}}) {{name}}]]
  local start = page
  -- check valid page range
  if(page == nil or page < 1) then
    print("ERROR: Invalid Page")
    return
  end
  -- print a page
  if(page == 1) then
    for i = start, SETTINGS.programs_per_page do
      local program = PROGRAMS[i]
      local menuentry = TEMPLATE:gsub("{{(%w+)}}",program)
      print(menuentry)
    end
  else
    start = ((page - 1) * SETTINGS.programs_per_page) + 1
    for i = start, (start + SETTINGS.programs_per_page) do
      local program = PROGRAMS[i]
      if(program ~= nil) then
        local program = PROGRAMS[i]
        local menuentry = TEMPLATE:gsub("{{(%w+)}}",program)
        print(menuentry)
      else
        break
      end
    end
  end
end

function menu()
  local selection = nil
  local page = 1
  -- menu loop
  repeat
    print("Please select a program\n")
    print("0) Close Repository Installer")
    showpage(page)
    print("> Next page \t\t < Previous page")
    selection = io.read("*l")
      
    repeat 
      if(selection == ">") then
        page = page + 1
        if(not checkPageNumber(page)) then page = 1 end
         -- reset input
        selection = nil
        do break end -- workaround for "continue"
      elseif(selection == "<") then
        page = page - 1
        if(page <= 0) then page = 1 end
        -- reset input
        selection = nil
        do break end -- workaround for "continue"
      else
        selection = tonumber(selection)
        assert(type(selection) == "number")
        do break end -- workaround for "continue"
      end
    until (true)
    
  until( selection ~= nil)
  -- check to cancel
  if(selection == 0) then
    print("Closing...")
    if(SETTINGS.debug) then
      -- Windows usage
      os.exit()
    else
      -- Computercraft usage
      shell.exit()
    end  
  end
  -- return app folder
  return PROGRAMS[tonumber(selection)].folder
end

---------------------------------------------------------------------
-- Execute Code
---------------------------------------------------------------------

header()
-- get desired app
local app = menu()
-- build app url
local baseurl = buildURL()
local url = baseurl .. app .. "/makefile"
-- create app structure and download app makefile
if(SETTINGS.debug) then
  -- Windows usage
  print("Download URL would be (sim): " .. url)
else -- Computercraft usage
  -- Create app directory and enter it
  if(fs.exists(fs.combine(shell.dir(), app))) then
    print("Updating application...")
    shell.run("cd", app)
    -- remove the old makefile, otherwise wget cant download the new makefile
    shell.run("rm","makefile")
  else
    print("Installing application...")
    shell.run("mkdir", app)
    shell.run("cd", app)   
  end
  -- Download app makefile
  shell.run("wget", url, "makefile")
  -- Execute app spectifc makefile
  shell.run("makefile")
end

