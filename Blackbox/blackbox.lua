BlackBox =
{
	-- BlackBox Variables
	bb_moveForward      = 1,
	bb_moveForwardText  = "Forward",
	bb_moveBack         = -1,
	bb_moveBackText     = "Back",
	bb_moveUp           = 2,
	bb_moveUpText       = "Up",
	bb_moveDown         = -2,
	bb_moveDownText     = "Down",
	bb_turnLeft         = 3,
	bb_turnLeftText     = "TurnLeft",
	bb_turnRight        = -3,
	bb_turnRightText    = "TurnRight",
	bb_lastIndex        = 0,
	bb_blackbox			= {},
	-- Other Variables
	simMode				= false,
	-- Exceptions
	exception_move		= 0
}


---------------------------------------------------------------------------------

--- Blackbox class constructor
-- @param o May a already created Blackbox object
-- @return Return the instance of the Blackbox object
function BlackBox:new(o)
    o = o or nil or {}
    setmetatable(o, self)
    self.__index = self
    return o
end


---------------------------------------------------------------------------------

--- Increase internal index
-- @return Return the new index
function BlackBox:nextIndex()
	self.bb_lastIndex = self.bb_lastIndex + 1
	return tonumber(self.bb_lastIndex)
end

--- Print exeception
-- @param e Exeception message
function BlackBox:exception(e)
	if ( e == self.exception_move ) then
		print("Exception: couldnt move !")
	end
end

--- Print all saved movements
function BlackBox:printBox()
	local b
	for b=1, self.bb_lastIndex do
		io.write(string.format("| %2d | ",self.bb_blackbox[b]))
		if b % 4 == 0 then
			io.write("\n")
		end
	end
	io.write("\n")
end

--- Enable or disable the simulation modus
-- The simulation mode print movement message instead of turtle movements
-- @param bool The boolean to configure the simulation mode.
function BlackBox:setSimMode(bool)
	self.simMode = bool
end

--- Convert a boolean to a string
-- @param The boolean to convert.
function BlackBox:BoolToString(bool)
	if ( bool == true ) then
		return "true"
	elseif ( bool == false ) then
		return "false"
	elseif ( bool == nil ) then
		return "nil"
	end
end

---------------------------------------------------------------------------------

--- Apply forward movement for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveForward()
	local ret = nil

	if ( self.simMode == true) then
		print("Move Forward (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_moveForward
		ret = true
	else
		ret = turtle.forward()
		if( ret == true) then
			self.bb_blackbox[self:nextIndex()] = self.bb_moveForward
		else
			BlackBox:exception(self.exception_move)
		end
	end
	return ret
end

--- Apply X steps of forward movements for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveXForward(steps)
	local retVal = true

	if(steps < 0) then
		for i = -steps, 1, -1 do
			retVal = (retVal and self:BoolToString(self:moveBack()))
		end
	else
		for i = 1, steps do
			retVal = (retVal and self:BoolToString(self:moveForward()))
		end
	end
	return retVal
end

--- Apply backwards movement for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveBack()
	local ret = nil

	if(self.simMode) then
		print("Move Back (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_moveBack
		ret = true
	else
		ret = turtle.back()
		if( ret == true ) then
			self.bb_blackbox[self:nextIndex()] = self.bb_moveBack
		else
			BlackBox:exception(self.exception_move)
		end
	end
	return ret
end

--- Apply X steps of backwards movements for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveXBack(steps)
	local retVal = true

	if(steps < 0) then
		for i = -steps, 1, -1 do
			retVal = (retVal and self:BoolToString(self:moveForward()))
		end
	else
		for i = 1, steps do
			retVal = (retVal and self:BoolToString(self:moveBack()))
		end
	end
	return retVal
end

--- Apply upwards movement for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveUp()
	local ret = nil

	if(self.simMode) then
		print("Move Up (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_moveUp
		ret = true
	else
		ret = turtle.up()
		if( ret == true ) then
			self.bb_blackbox[self:nextIndex()] = self.bb_moveUp
		else
			BlackBox:exception(self.exception_move)
		end
	end
	return ret
end

--- Apply X steps of upwards movements for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveXUp(steps)
	local retVal = true

	if(steps < 0) then
		for i = -steps, 1, -1 do
			retVal = (retVal and self:BoolToString(self:moveDown()))
		end
	else
		for i = 1, steps do
			retVal = (retVal and self:BoolToString(self:moveUp()))
		end
	end
	return retVal
end

--- Apply downwards movement for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveDown()
	local ret = nil

	if(self.simMode) then
		print("Move Down (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_moveDown
		ret = true
	else
		ret = turtle.down()
		if( ret == true ) then
			self.bb_blackbox[self:nextIndex()] = self.bb_moveDown
		else
			BlackBox:exception(self.exception_move)
		end
	end
	return ret
end

--- Apply X steps of downwards movements for a turtle
-- @return Return a boolean of the successed of this operation
function BlackBox:moveXDown(steps)
	local retVal = true

	if(steps < 0) then
		for i = -steps, 1, -1 do
			retVal = (retVal and self:BoolToString(self:moveUp()))
		end
	else
		for i = 1, steps do
			retVal = (retVal and self:BoolToString(self:moveDown()))
		end
	end
	return retVal
end


---------------------------------------------------------------------------------

--- Apply turn left movement for a turtle
function BlackBox:turnLeft()
	if(self.simMode) then
		print("Turn Left (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_turnLeft
	else
		turtle.turnLeft()
		self.bb_blackbox[self:nextIndex()] = self.bb_turnLeft
	end
end

--- Apply turn right movement for a turtle
function BlackBox:turnRight()
	if(self.simMode) then
		print("Turn Right (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_turnRight
	else
		turtle.turnRight()
		self.bb_blackbox[self:nextIndex()] = self.bb_turnRight
	end
end

--- Apply a 180 degree movement for a turtle
function BlackBox:turnAround()
	if(self.simMode) then
		print("Turn Around (sim)")
		self.bb_blackbox[self:nextIndex()] = self.bb_turnRight
		self.bb_blackbox[self:nextIndex()] = self.bb_turnRight
	else
		BlackBox:turnRight()
		BlackBox:turnRight()
	end
end


---------------------------------------------------------------------------------

--- Clear the internal blackbox memory
function BlackBox:cleanBB()
	self.bb_blackbox = {}
	self.bb_lastIndex = 0
end

--- Execute the opposite movement operation of the given movement
-- @param action Takes a action enumeration which is declared in this class.
function BlackBox:reverse(action)
	if(action == nil) then
		return
	end	
	
	local ret = (0 - tonumber(action))

	if ( ret == self.bb_moveForward ) then
		self:moveForward()
	elseif ( ret == self.bb_moveBack ) then
		self:moveBack()
	elseif ( ret == self.bb_moveUp ) then
		self:moveUp()
	elseif ( ret == self.bb_moveDown ) then
		self:moveDown()
	elseif ( ret == self.bb_turnLeft ) then
		self:turnLeft()
	elseif ( ret == self.bb_turnRight ) then
		self:turnRight()
	end
end

--- Execute all opposite operation of all saved movements
function BlackBox:reverseLoop()
	local b
	for b=self.bb_lastIndex, 1, -1 do
		self:reverse(self.bb_blackbox[b])
	end
end



---------------------------------------------------------------------------------

--[[ Example 1
test = BlackBox:new()
test:moveForward()
test:moveBack()
test:moveUp()
test:moveDown()
test:turnLeft()
test:turnRight()
test:turnAround()
test:printBox()
test:reverseLoop()
test:printBox()
--]]
