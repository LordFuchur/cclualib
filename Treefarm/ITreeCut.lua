#!/usr/bin/lua5.1
-- Interface for different TreeCut implementations.
-- Implementions of this interface must at least implement the followed defined functions as prototypes.
-- If your class implements this interface you should mark it with:
-- "-- implement ITreeCut"

-- Start of Interface Defintion

--- Init function for TreeCut interface.
-- @param params Take optional parameters, but the programmer need to know what he will get.
function Init(params)
  error("Tried to execute TreeCut interface!")
end

--- Cut a tree (stand in front of the stomp)
-- @param saplingSlot int A slot which contain a sapling to replant
function CutTree(saplingSlot)
  error("Tried to execute TreeCut interface!")
end

-- End of Interface Defintion

--- Can be used to check if a certain object is an implementation of this interface
-- @param obj_toCheck The object which shall be checked
function CheckisInterfaceImplementation(obj_toCheck)
    if(obj_toCheck == nil) then
        return false
    end
    if(type(obj_toCheck.Init) == nil)then
        return false
    end
    if(type(obj_toCheck.CutTree) == nil)then
        return false
    end
    return true
end