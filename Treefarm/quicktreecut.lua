#!/usr/bin/lua5.1

-- import ProxyTurtle
os.loadAPI('Treefarm/proxyturtle.lua')
-- set alias
PT = proxyturtle

--implement ITreeCut
QuickTreeCut = {}
QuickTreeCut.__index = QuickTreeCut

--- QuickTreeCut class constructor
-- @param pTurtle An instance of a proxyturtle
-- @return Return the instance of the QuickTreeCut object
function QuickTreeCut:new(pTurtle)
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,QuickTreeCut)
  -- initialize instance
  instance:Init()
  -- In this implementation of ITreeCut I know that I will get a proxyturtle object.
  if(pTurtle == nil) then
    instance.pTurtle = PT.ProxyTurtle:new()
  else
    instance.pTurtle = pTurtle
  end
  -- return the new instance of the class
  return instance
end  

--ITreeCut implementations start

--- Init interface function implementation of ITreeCut.
-- @param params No usage in this implementation
function QuickTreeCut:Init(params)

end

--- CutTree interface function implementation of ITreeCut.
-- Cut a tree (stand in front of the stomp)
-- @param saplingSlot int A slot which contain a sapling to replant
function QuickTreeCut:CutTree(saplingSlot)
  local count = 0
  -- into the tree
  self.pTurtle:dig()
  self.pTurtle:forward()
  -- cut root
  local success, data = turtle.inspectDown()
  if(success and data.name ~= "minecraft:sapling") then
    self.pTurtle:digDown()
  end
  -- cut upwards
  while(self.pTurtle:detectUp()) do
    self.pTurtle:digUp()
    self.pTurtle:up()
    count = count + 1
  end
  -- move down above root
  for move = 1, count do
    self.pTurtle:down()
  end
  if(saplingSlot ~= 0) then
    self.pTurtle:select(saplingSlot)
    self.pTurtle:placeDown()
  end
end

--ITreeCut implementations end

--[[  Example 1 
local test = QuickTreeCut:new()
test:CutTree()
 --]]