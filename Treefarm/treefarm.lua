#!/usr/bin/lua5.1

-- imports
os.loadAPI('Treefarm/ITreeCut.lua')
os.loadAPI('Treefarm/quicktreecut.lua')
os.loadAPI('Treefarm/proxyturtle.lua')
os.loadAPI('Treefarm/advtreecut.lua')
-- set alias
ITC = ITreeCut
QTC = quicktreecut
ATC = advtreecut
PT  = proxyturtle

TreeFarm = 
{
  saplingMinSlot = 1,
  saplingMaxSlot = 4,
  lightBlockSlot = 13,
  blockSlot = 14,
  chestSlot = 15,
}

-- Sapling Id strings are currently for MC version 1.12.2.
-- In version > 1.13 they got unique names as "minecraft:jungle_sapling"
local SaplingDictionary = 
{
  {Name="Oak",      IdString="minecraft:sapling",DataValue=0},
  {Name="Spruce",   IdString="minecraft:sapling",DataValue=1},
  {Name="Birch",    IdString="minecraft:sapling",DataValue=2},
  {Name="Jungle",   IdString="minecraft:sapling",DataValue=3},
  {Name="Acacia",   IdString="minecraft:sapling",DataValue=4},
  {Name="DarkOak",  IdString="minecraft:sapling",DataValue=5},
}

-- Leaves Id strings are currently for MC version 1.12.2.
-- In version > 1.13 they got unique names as "minecraft:acacia_leaves"
local LeavesDictionary = 
{
  {Name="OakLeaves",      IdString="minecraft:leaves",DataValue=0},
  {Name="SpruceLeaves",   IdString="minecraft:leaves",DataValue=1},
  {Name="BirchLeaves",    IdString="minecraft:leaves",DataValue=2},
  {Name="JungleLeaves",   IdString="minecraft:leaves",DataValue=3},
  {Name="AcaciaLeaves",   IdString="minecraft:leaves2",DataValue=0},
  {Name="DarkOakLeaves",  IdString="minecraft:leaves2",DataValue=1},
}

-- Log Id strings are currently for MC version 1.12.2.
-- In version > 1.13 they got unique names as "minecraft:acacia_log"
local LogDictionary = 
{
  {Name="OakWood",      IdString="minecraft:log",DataValue=0},
  {Name="SpruceWood",   IdString="minecraft:log",DataValue=1},
  {Name="BirchWood",    IdString="minecraft:log",DataValue=2},
  {Name="JungleWood",   IdString="minecraft:log",DataValue=3},
  {Name="AcaciaWood",   IdString="minecraft:log2",DataValue=0},
  {Name="DarkOakWood",  IdString="minecraft:log2",DataValue=1},
}

TreeFarm.__index = TreeFarm

--- TreeFarm class constructor
-- @param treeOffset int The offset between two trees.
-- @param chestOffset int The offset between the chest and the first tree.
-- @param treeCut ITreeCut An object which implements ITreeCut.
-- @param pTurtle ProxyTurtle Optional A proxyturtle object.
-- @return Return the instance of the TreeFarm object
function TreeFarm:new(treeOffset,chestOffset,treeCut,pTurtle)
  -- check parameters
  -- TODO: Test interface check and add the other checks
  if(not ITC.CheckisInterfaceImplementation(treeCut)) then
    error("treeCut isnt a implementation of ITreeCut")
  end
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,TreeFarm)
  -- initialize instance
    -- create own proxyturtle when no given
  if(pTurtle == nil) then
    pTurtle = PT.ProxyTurtle:new()
  end
  instance.pTurtle = pTurtle
  instance.treeOffset = treeOffset
  instance.chestOffset = chestOffset
  instance.treeCut = treeCut
  instance.lastRunTime = os.clock()
  -- return the new instance of the class
  return instance
end

--- Find the next slot with a sapling in it
-- @return int Slot number of a slot with a sapling otherwise return 0.
function TreeFarm:findSaplingSlot()
  for i = self.saplingMinSlot, self.saplingMaxSlot do
    self.pTurtle:select(i)
    for s,sap in ipairs(SaplingDictionary) do
        local data = self.pTurtle:getItemDetail(i)
        if(data ~= nil) then
          if(sap.IdString == data.name) then
            return i
          end
        end
    end
  end
  print("ERROR: Couldnt found saplings !")
  return 0
end

--- Movement routine to place the management chest for the farm
function TreeFarm:setupChest()
  self.pTurtle:select(TreeFarm.chestSlot)
  self.pTurtle:digDown()
  self.pTurtle:turnRight()
  self.pTurtle:forward()
  self.pTurtle:digDown()
  self.pTurtle:placeDown()
  self.pTurtle:turnLeft()
  self.pTurtle:turnLeft()
  self.pTurtle:forward()
  self.pTurtle:turnRight()
  self.pTurtle:placeDown()
end

--- Build routine for one sapling spot
function TreeFarm:setupTreeSpot()
    -- [ ] [ ] [ ] 
    -- [ ] [T] [ ]
    -- [ ] [X] [ ]
    self.pTurtle:back()
    self.pTurtle:digDown()
    self.pTurtle:select(self.blockSlot)
    self.pTurtle:placeDown()
    self.pTurtle:forward()
    -- [ ] [X] [ ] 
    -- [ ] [T] [ ]
    -- [ ] [ ] [ ]
    self.pTurtle:forward()
    self.pTurtle:digDown()
    self.pTurtle:select(self.lightBlockSlot)
    if(self.pTurtle:getItemCount() > 0) then
      self.pTurtle:placeDown()
    else
      self.pTurtle:select(self.blockSlot)
      self.pTurtle:placeDown()
    end
    self.pTurtle:back()
    -- [ ] [ ] [ ] 
    -- [ ] [T] [X]
    -- [ ] [ ] [ ]
    self.pTurtle:turnRight()
    self.pTurtle:forward()
    self.pTurtle:digDown()
    self.pTurtle:select(self.blockSlot)
    self.pTurtle:placeDown()    
    self.pTurtle:back()
    self.pTurtle:turnLeft()
    -- [ ] [ ] [ ] 
    -- [X] [T] [ ]
    -- [ ] [ ] [ ]
    self.pTurtle:turnLeft()
    self.pTurtle:forward()
    self.pTurtle:digDown()
    self.pTurtle:select(self.lightBlockSlot)
    if(self.pTurtle:getItemCount() > 0) then
      self.pTurtle:placeDown()
    else
      self.pTurtle:select(self.blockSlot)
      self.pTurtle:placeDown()
    end    
    self.pTurtle:back()
    self.pTurtle:turnRight()
    -- if turtle find saplings in inventory place it, otherwise skip it.
    local sapSlot = self:findSaplingSlot()
    if(sapSlot ~= 0) then
      self.pTurtle:up()
      self.pTurtle:select(sapSlot)
      self.pTurtle:placeDown()
      self.pTurtle:forward()
      self.pTurtle:down()
    else
      self.pTurtle:forward()
    end
end 

--- Movement routine to move between management chest and first treespot
function TreeFarm:moveChestOffset()
  for w = 1,self.chestOffset do
    self.pTurtle:forward()
  end
end
--- Movement routine to move between two trees
function TreeFarm:moveTreeOffset()
  for w = 1,self.treeOffset do
    self.pTurtle:forward()
  end
end
--- Movement routine to switch to the next colomn
-- @param moveLeft bool true to move to the left colomn or otherwise
-- @param isBuilding bool true if executed in building mode otherwise when used to check the field
function TreeFarm:moveNextColomn(moveLeft,isBuilding)
  if(moveLeft) then
    self.pTurtle:turnLeft()
    self:moveTreeOffset()
    self.pTurtle:forward()
    self.pTurtle:turnLeft()
    if(isBuilding) then
      self.pTurtle:forward()
    end
  else
    self.pTurtle:turnRight()
    self:moveTreeOffset()
    self.pTurtle:forward()
    self.pTurtle:turnRight()
    if(isBuilding) then
      self.pTurtle:forward()
    end
  end
end

--- Drive the turtle back over the management chest
-- @param farmRows The amount of rows of the treefarm
-- @param farmColomn The amount of colomns of the treefarm
function TreeFarm:driveBack(farmRows,farmColomns)
    local isEven = ((farmColomns % 2) == true)
    
    if(not isEven) then
      -- turn around and stand beside the tree
      self.pTurtle:turnRight()
      self.pTurtle:forward()
      self.pTurtle:turnRight()
      self.pTurtle:forward()
      -- drive all the way to the end of the farm
      for r = 1, (farmRows - 1) do
          self:moveTreeOffset()
      end
      for t = 1, farmRows do
        self.pTurtle:forward()
      end
      self.pTurtle:turnLeft()
      self.pTurtle:back()
    else
      self.pTurtle:turnLeft()
    end
    -- move back to chest colomn
    for r = 1, (farmColomns - 1) do
      self:moveTreeOffset()
      self.pTurtle:forward()
    end
    -- move back over management chest
    self.pTurtle:turnRight()
    self:moveChestOffset()
    self.pTurtle:turnLeft()
    self.pTurtle:turnLeft()
end

--- Empty all logs out of the turtle
function TreeFarm:emptyTurtle()
  for s = 1, 16 do
      self.pTurtle:select(s)
      -- for each log type
      for l,log in ipairs(LogDictionary) do
        local data = self.pTurtle:getItemDetail()
        if(data ~= nil) then
          -- drop log into chest
          if(log.IdString == data.name) then
            self.pTurtle:dropDown()
          end
        end       
      end
  end
end
--- Build a new Treefarm with the given dimensions.
-- The turtle build the farm from the bottom right corner in the top left direction.
-- @param rows int Amount of rows
-- @param colomn int Amount of colomns
function TreeFarm:Build(rows,colomn)
  local side = true
  -- check materials
  self:checkBuildMaterials(rows,colomn)
  -- setup management chest
  self:setupChest()
  -- move to the first treespot
  self:moveChestOffset()
  self.pTurtle:forward()
  for col = 1, colomn do
    -- build rows
    for row = 1, rows do
      self:setupTreeSpot()
      if(row < rows) then
        self:moveTreeOffset()
      end
    end
    -- move next colomn
    if(col < colomn) then
      self:moveNextColomn(side,true)
    end
    -- toggle side
    side = not side
  end -- end for (col)
  self:driveBack(rows,colomn)
end -- end func

--- Check for enough build materials to build the treefarm
function TreeFarm:checkBuildMaterials(rows,colomn)
  local blocks    = (rows * colomn) * 4
  local saplings  = (rows * colomn)
  local chests    = 2
  local cBlocks   = self.pTurtle:getItemCount(self.blockSlot)
  local cChests   = self.pTurtle:getItemCount(self.chestSlot)
  local cSaplings = 0
  -- TODO: check if they are really saplings
  for ss = self.saplingMinSlot, self.saplingMaxSlot do
    local temp = self.pTurtle:getItemCount(ss)
    if(temp ~= nil) then
      cSaplings = cSaplings + temp
    end
  end
  
  while(cBlocks < blocks or cSaplings < saplings or cChests < chests) do
    print("Please fill up build materials.")
    print("Blocks Slot: " .. self.blockSlot)
    print("Chest Slot: " .. self.chestSlot)
    print("Light block Slot: " .. self.lightBlockSlot)
    print("Sapling Slots: " .. self.saplingMinSlot .. " to " .. self.saplingMaxSlot)
    print("Blocks need: " .. blocks)
    print("Chest need: " .. chests)
    print("Light block need: " .. (blocks / 2))
    print("Sapling need: " .. saplings)    
    print("Press ENTER to check again...")
    io.read()
    -- check for materials
    cBlocks   = self.pTurtle:getItemCount(self.blockSlot)
    cChests   = self.pTurtle:getItemCount(self.chestSlot)
    cSaplings = 0
    for ss = self.saplingMinSlot, self.saplingMaxSlot do
      local temp = self.pTurtle:getItemCount(ss)
      if(temp ~= nil) then
        cSaplings = cSaplings + temp
      end
    end
  end
end  

--- Check for grown trees
-- @param rows int The rows of the tree farm
-- @param colomn int The colomns of the tree farm
-- @param threshold int Realife seconds
function TreeFarm:Check(rows,colomn,threshold)
  local skip = (threshold <= 0)
  local isTime = self:checkTime(threshold)
  print("Last: " .. self.lastRunTime .. " | " .. tostring(isTime))
  -- perma loop check to execute
  while(true) do
    isTime = self:checkTime(threshold)
    -- the check itself
    if(isTime or skip) then
      self.pTurtle:up()
      local side = true
      -- move to the first treespot
      self:moveChestOffset()
      for col = 1, colomn do
        -- build rows
        for row = 1, rows do
          local saplingSlot = self:findSaplingSlot()
          self.treeCut:CutTree(saplingSlot)
          if(row < rows) then
            self:moveTreeOffset()
          else
            self.pTurtle:forward()
          end
        end
        -- move next colomn
        if(col < colomn) then
          self:moveNextColomn(side,false)
        end
        -- toggle side
        side = not side
      end -- end for (col)
      self:driveBack(rows,colomn)
      self.pTurtle:down()
      self:emptyTurtle()
      -- update last check time
      self.lastRunTime = os.clock()
      -- set skip to false in case it was just a one time use
      if(skip) then
        break
      end
    end -- end if
    sleep(30)
  end -- end while
end

--- Returns if a threshold is reached or not. 
-- Based on the lastRunTime variable inside of the instance.
-- @param threshold int Realife seconds
-- @return bool True if threshold reached otherwise false
function TreeFarm:checkTime(threshold)
    local diff = os.clock() - self.lastRunTime
    if(diff >= threshold) then
      return true
    end
    return false
end

--[[  Example 1  

function treeFarmForward()
  if(turtle.detect()) then
    turtle.dig()
  end
  turtle.forward()
end

local pT = PT.ProxyTurtle:new()
local tC = QTC.QuickTreeCut:new()
pT:SetProxyFunction("pt_forward",treeFarmForward)
local test = TreeFarm:new(6,3,tC,pT)
--test:Build(2,3)
test:Check(2,3,60)
--]]

--[[  Example 2 --]]

function treeFarmForward()
  if(turtle.detect()) then
    turtle.dig()
  end
  turtle.forward()
end

local pT = PT.ProxyTurtle:new()
local tC = ATC.AdvTreeCut:new()
pT:SetProxyFunction("pt_forward",treeFarmForward)
local test = TreeFarm:new(6,3,tC,pT)
--test:Build(2,3)
test:Check(2,3,0)

