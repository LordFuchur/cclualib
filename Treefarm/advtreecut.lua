#!/usr/bin/lua5.1

-- import ProxyTurtle
os.loadAPI('Treefarm/proxyturtle.lua')
-- set alias
PT = proxyturtle

-- Leaves Id strings are currently for MC version 1.12.2.
-- In version > 1.13 they got unique names as "minecraft:acacia_leaves"
local LeavesDictionary = 
{
  {Name="OakLeaves",      IdString="minecraft:leaves",DataValue=0},
  {Name="SpruceLeaves",   IdString="minecraft:leaves",DataValue=1},
  {Name="BirchLeaves",    IdString="minecraft:leaves",DataValue=2},
  {Name="JungleLeaves",   IdString="minecraft:leaves",DataValue=3},
  {Name="AcaciaLeaves",   IdString="minecraft:leaves2",DataValue=0},
  {Name="DarkOakLeaves",  IdString="minecraft:leaves2",DataValue=1},
}

-- Log Id strings are currently for MC version 1.12.2.
-- In version > 1.13 they got unique names as "minecraft:acacia_log"
local LogDictionary = 
{
  {Name="OakWood",      IdString="minecraft:log",DataValue=0},
  {Name="SpruceWood",   IdString="minecraft:log",DataValue=1},
  {Name="BirchWood",    IdString="minecraft:log",DataValue=2},
  {Name="JungleWood",   IdString="minecraft:log",DataValue=3},
  {Name="AcaciaWood",   IdString="minecraft:log2",DataValue=0},
  {Name="DarkOakWood",  IdString="minecraft:log2",DataValue=1},
}


--implement ITreeCut
AdvTreeCut = {}
AdvTreeCut.__index = AdvTreeCut

--- QuickTreeCut class constructor
-- @param pTurtle An instance of a proxyturtle
-- @return Return the instance of the QuickTreeCut object
function AdvTreeCut:new(pTurtle)
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,AdvTreeCut)
  -- initialize instance
  instance:Init()
  -- In this implementation of ITreeCut I know that I will get a proxyturtle object.
  if(pTurtle == nil) then
    instance.pTurtle = PT.ProxyTurtle:new()
  else
    instance.pTurtle = pTurtle
  end
  -- return the new instance of the class
  return instance
end  

--- 
function AdvTreeCut:cutBranch()
  print("cutBranch")
  local count = 0
  -- check block in front of turtle
  local success, data = self.pTurtle:inspect()
  print(success)
  if(not success) then return end
  repeat
    print("Loop")
    if(success) then 
      -- check every registered wood type
      for l,log in ipairs(LogDictionary) do
        -- if wood food, cut branch
        if(log.IdString == data.name) then
          -- cut wood and move forward
          self.pTurtle:dig()
          self.pTurtle:forward()
          count = count + 1
          -- cut leaves (new saplings)
          self.pTurtle:digUp()
          self.pTurtle:digDown()
          self.pTurtle:turnLeft()
          self.pTurtle:dig()
          self.pTurtle:turnAround() -- 180 degree
          self.pTurtle:dig()
          self.pTurtle:turnLeft()
          -- check next branch block
          success, data = self.pTurtle:inspect()
        else
          -- just cut leaves (new saplings)
          self.pTurtle:dig()
          success = false
        end
      end -- end for logs
    else
      success = false
    end -- end if success
  until(success)
  print("cutBranch - move back")  
  -- move back into stomp
  for move = 1, count do
    self.pTurtle:back()
  end
end

--- 
function AdvTreeCut:checkTreeLayer()
  print("CheckTreeLayer")
  for s = 1, 4 do
    self:cutBranch()
    -- turn to the next side
    self.pTurtle:turnLeft()
  end -- end for sides
end

--ITreeCut implementations start

--- Init interface function implementation of ITreeCut.
-- @param params No usage in this implementation
function AdvTreeCut:Init(params)

end

--- CutTree interface function implementation of ITreeCut.
-- Cut a tree (stand in front of the stomp)
-- @param saplingSlot int A slot which contain a sapling to replant
function AdvTreeCut:CutTree(saplingSlot)
  local count = 0
  -- into the tree
  self.pTurtle:dig()
  self.pTurtle:forward()
  -- cut root
  local success, data = turtle.inspectDown()
  if(success and data.name ~= "minecraft:sapling") then
    self.pTurtle:digDown()
  end
  -- cut upwards
  while(self.pTurtle:detectUp()) do  
    self.pTurtle:digUp()
    self.pTurtle:up()
    -- check tree on this height
    self:checkTreeLayer()
    count = count + 1  
  end
  -- move down above root
  for move = 1, count do
    self.pTurtle:down()
  end
  if(saplingSlot ~= 0) then
    self.pTurtle:select(saplingSlot)
    self.pTurtle:placeDown()
  end
end

--ITreeCut implementations end