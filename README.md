# CCLuaLib

CCLuaLib is a collection of Lua applications for the Minecraft Mod Computercraft.

### Tech

CCLuaLib may use a number of different project and technics.

* [LuaForWindows] - Lua 5.1 Installtion for Windows. (LuaDoc is already integrated.)
* [LuaDoc] - Lua Documentation script
* [Powershell] - Windows PowerShell is a Windows command-line shell designed especially for system administrators.

### Installation

Like Computercraft the Lua scripts require Lua 5.1.

First you have to download the Installer.lua to a Computer or Turtle.
For this you start the lua interpreter and run the download command.

```lua
lua
> shell.run("wget","https://gitlab.com/LordFuchur/cclualib/raw/master/Installer.lua","Installer.lua")
```

Another way would be to create firstly a file to execute command. 
Sometimes its better to do this, cause you can paste the URL.

```lua
edit SomeFileName.lua
```
Insert the same command as in the first method.
Here you can STRG-V the URL into the in-game editor.

```lua
shell.run("wget","https://gitlab.com/LordFuchur/cclualib/raw/master/Installer.lua","Installer.lua")
```

Save the file with STRG and then "save" and ENTER.
Execute the file

```lua
SomeFileName.lua
```

After the Installer is downloaded, you can execute it.

```lua
Installer.lua
```

In the following is an example output like it could be shown.

```lua
------------------------------
---> Repository Installer <---
------------------------------

Please select a program

0) Close Repository Installer
1) Blackbox
2) Stationfarm
```

Now you just need to chose the desired application to download.


### Troubleshoot




[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. 
There is no need to format nicely because it shouldn't be seen. 
Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

[LuaForWindows]: <https://github.com/rjpcomputing/luaforwindows>
[LuaDoc]: <http://keplerproject.github.io/luadoc/index.html#overview>
[Powershell]: <https://docs.microsoft.com/de-de/powershell/scripting/getting-started/getting-started-with-windows-powershell?view=powershell-6>
[Powershell2]: <https://www.windowspro.de/andreas-kroschel/powershell-executionpolicy-setzen-scripts-signieren-und-ausfuehren>