#!/usr/bin/lua5.1

ProxyTurtle = {}

---------------------------------------------------------------------------------

--- ProxyTurtle class constructor
-- @return Return the instance of the ProxyTurtle object
function ProxyTurtle:new()
  -- create new instance
  local instance = {}
  -- link new instance to the namespace
  setmetatable(instance,ProxyTurtle)
  self.__index = self
  -- create empty turtle function table
  local turtleFuncTable = 
  {
    pt_craft = nil,
    pt_forward = nil,
    pt_back = nil,    
    pt_up = nil,
    pt_down = nil,
    pt_turnRight = nil,
    pt_turnLeft = nil,
    pt_turnAround = nil,
    pt_select = nil,
    pt_getSelectedSlot = nil,
    pt_getItemCount = nil,
    pt_getItemSpace = nil,
    pt_getItemDetail = nil,
    pt_equipLeft = nil,
    pt_equipRight = nil,
    pt_attack = nil,
    pt_attackUp = nil,
    pt_attackDown = nil,
    pt_dig = nil,
    pt_digUp = nil,
    pt_digDown = nil,
    pt_place = nil,
    pt_placeUp = nil,
    pt_placeDown = nil,
    pt_detect = nil,
    pt_detectUp = nil,
    pt_detectDown = nil,
    pt_inspect = nil,
    pt_inspectUp = nil,
    pt_inspectDown = nil,
    pt_compare = nil,
    pt_compareUp = nil,
    pt_compareDown = nil,
    pt_compareTo = nil,
    pt_drop = nil,
    pt_dropUp = nil,
    pt_dropDown = nil,
    pt_suck = nil,
    pt_suckUp = nil,
    pt_suckDown = nil,
    pt_refuel = nil,
    pt_getFuelLevel = nil,
    pt_getFuelLimit = nil,
    pt_transferTo = nil,
  }
  -- set empty function table to instance
  instance.pt_turtleFuncTable = turtleFuncTable
  -- return the new instance of the class
  return instance
end

---------------------------------------------------------------------------------

--- Setup a proxy function for a turtle function
-- @param funcName string The name of the turtle function with "pt_" in front of it.
-- Example: ProxyTurtleInstance:SetProxyFunction("pt_dig",funcPointer)
-- @param funcPointer A pointer to the substitution function for turtle function (funcName)
function ProxyTurtle:SetProxyFunction(funcName,funcPointer)
  self.pt_turtleFuncTable[funcName] = funcPointer
end

---------------------------------------------------------------------------------

--- Craft items using ingredients anywhere in the turtle's inventory and place results in the active slot. 
-- If a quantity is specified, it will craft only up to that many items, otherwise, it will craft as many of the items as possible.
-- @param quantity Specify the quantity of items to craft. 
-- If the quantity specified is 0, will return true if a valid recipe has been found in the turtle's inventory, and false otherwise.
-- @return boolean if an item was crafted
function ProxyTurtle:craft(quantity)
  if(self.pt_turtleFuncTable.pt_craft == nil) then
    return turtle.craft(quantity)
  else
    return self.pt_turtleFuncTable.pt_craft(quantity)
  end
end

---------------------------------------------------------------------------------

--- Attempts to move the turtle forward.
-- @return boolean whether the turtle succeeded in moving forward
function ProxyTurtle:forward()
  if(self.pt_turtleFuncTable.pt_forward == nil) then
    return turtle.forward()
  else
    return self.pt_turtleFuncTable.pt_forward()
  end
end

---------------------------------------------------------------------------------

--- Attempts to move the turtle backward.
-- @return boolean whether the turtle succeeded in moving backwards
function ProxyTurtle:back()
  if(self.pt_turtleFuncTable.pt_back == nil) then
    return turtle.back()
  else
    return self.pt_turtleFuncTable.pt_back()
  end
end

---------------------------------------------------------------------------------

--- Attempts to move the turtle upwards.
-- @return 	boolean whether the turtle succeeded in moving upwards.
function ProxyTurtle:up()
  if(self.pt_turtleFuncTable.pt_up == nil) then
    return turtle.up()
  else
    return self.pt_turtleFuncTable.pt_up()
  end
end

---------------------------------------------------------------------------------

--- Attempts to move the turtle downwards.
-- @return 	boolean whether the turtle succeeded in moving downwards.
function ProxyTurtle:down()
  if(self.pt_turtleFuncTable.pt_down == nil) then
    return turtle.down()
  else
    return self.pt_turtleFuncTable.pt_down()
  end
end

---------------------------------------------------------------------------------

--- Attempts to turn the turtle left.
-- @return 	boolean whether the turtle succeeded in turning left.
function ProxyTurtle:turnLeft()
  if(self.pt_turtleFuncTable.pt_turnLeft == nil) then
    return turtle.turnLeft()
  else
    return self.pt_turtleFuncTable.pt_turnLeft()
  end
end

---------------------------------------------------------------------------------

--- Attempts to turn the turtle right.
-- @return 	boolean whether the turtle succeeded in turning right.
function ProxyTurtle:turnRight()
  if(self.pt_turtleFuncTable.pt_turnRight == nil) then
    return turtle.turnRight()
  else
    return self.pt_turtleFuncTable.pt_turnRight()
  end
end

---------------------------------------------------------------------------------

--- Attempts to turn the turtle around (180 degree). 
-- Use by default two times turtle.turnRight.
-- @return 	boolean whether the turtle succeeded in turning around.
function ProxyTurtle:turnAround()
  if(self.pt_turtleFuncTable.pt_turnAround == nil) then
    return (turtle.turnRight() and turtle.turnRight())
  else
    return self.pt_turtleFuncTable.pt_turnAround()
  end
end

---------------------------------------------------------------------------------

--- Attempts to select the specified slot.
-- @param slotNumber (1-16 in ComputerCraft 1.4+, otherwise 1-9)
-- @return 	boolean whether the slot number specified is within the accepted range or not.
function ProxyTurtle:select(slotNumber)
  if(self.pt_turtleFuncTable.pt_select == nil) then
    return turtle.select(slotNumber)
  else
    return self.pt_turtleFuncTable.pt_select(slotNumber)
  end
end

---------------------------------------------------------------------------------

--- Added by version 1.6 of ComputerCraft, this command returns the currently selected inventory slot. In builds prior this value must be tracked manually by the scripter.
-- @return number selected slot
function ProxyTurtle:getSelectedSlot()
  if(self.pt_turtleFuncTable.pt_getSelectedSlot == nil) then
    return turtle.getSelectedSlot()
  else
    return self.pt_turtleFuncTable.pt_getSelectedSlot()
  end
end

---------------------------------------------------------------------------------

--- Return the number of items in the specified slot
-- @param slotNumber The slot number which shall be used.
-- @return number the number of items found in the specified slot.
function ProxyTurtle:getItemCount(slotNumber)
  if(self.pt_turtleFuncTable.pt_getItemCount == nil) then
    return turtle.getItemCount(slotNumber)
  else
    return self.pt_turtleFuncTable.pt_getItemCount(slotNumber)
  end
end

---------------------------------------------------------------------------------

--- Prints the number of spaces remaining in the specified slot.
-- @param slotNumber The slot number which shall be used.
-- @return number the amount of room for items found in the specified slot.
function ProxyTurtle:getItemSpace(slotNumber)
  if(self.pt_turtleFuncTable.pt_getItemSpace == nil) then
    return turtle.getItemSpace(slotNumber)
  else
    return self.pt_turtleFuncTable.pt_getItemSpace(slotNumber)
  end
end

---------------------------------------------------------------------------------

--- Returns the ID string, count and damage values of currently selected slot or, 
-- if specified, slotNum slot in a table format: 
-- { name = "modname:itemname", damage = 0, count = 1 }. 
-- Returns nil if there is no item in the specified or currently selected slot.
-- @param slotNumber The slot number which shall be used.
-- @return table data 
function ProxyTurtle:getItemDetail(slotNumber)
  if(self.pt_turtleFuncTable.pt_getItemDetail == nil) then
    return turtle.getItemDetail(slotNumber)
  else
    return self.pt_turtleFuncTable.pt_getItemDetail(slotNumber)
  end
end

---------------------------------------------------------------------------------

--- As of ComputerCraft 1.6, turtles may have items added to (or even removed from!) them outside of the crafting grid.
-- If a valid item exists in the currently selected inventory slot, this command places it on the turtle's left side. 
-- If an item was already present there, it'll be placed back into the inventory in the currently selected slot 
-- (assuming it's not taken up by eg an item that the turtle couldn't equip). 
-- Both actions can be performed in one go, allowing turtles to instantly swap tools.
-- @return true, or false if an invalid item was in the selected inventory slot.
function ProxyTurtle:equipLeft()
  if(self.pt_turtleFuncTable.pt_equipLeft == nil) then
    return turtle.equipLeft()
  else
    return self.pt_turtleFuncTable.pt_equipLeft()
  end
end

---------------------------------------------------------------------------------

--- As of ComputerCraft 1.6, turtles may have items added to (or even removed from!) them outside of the crafting grid.
-- If a valid item exists in the currently selected inventory slot, this command places it on the turtle's right side. 
-- If an item was already present there, it'll be placed back into the inventory in the currently selected slot 
-- (assuming it's not taken up by eg an item that the turtle couldn't equip). 
-- Both actions can be performed in one go, allowing turtles to instantly swap tools.
-- @return true, or false if an invalid item was in the selected inventory slot.
function ProxyTurtle:equipRight()
  if(self.pt_turtleFuncTable.pt_equipRight == nil) then
    return turtle.equipRight()
  else
    return self.pt_turtleFuncTable.pt_equipRight()
  end
end

---------------------------------------------------------------------------------

--- Attempts to attack in front of the turtle. 
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to attack with, 
-- otherwise the turtle will attempt to attack with both tools.
-- @return boolean whether the turtle succeeded in attacking forward
function ProxyTurtle:attack(toolSide)
  if(self.pt_turtleFuncTable.pt_attack == nil) then
    return turtle.attack(toolSide)
  else
    return self.pt_turtleFuncTable.pt_attack(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Attempts to attack above the turtle.
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to attack with, 
-- otherwise the turtle will attempt to attack with both tools.
-- @return boolean whether the turtle succeeded in attacking upwards
function ProxyTurtle:attackUp(toolSide)
  if(self.pt_turtleFuncTable.pt_attackUp == nil) then
    return turtle.attackUp(toolSide)
  else
    return self.pt_turtleFuncTable.pt_attackUp(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Attempts to attack below the turtle.
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to attack with, 
-- otherwise the turtle will attempt to attack with both tools.
-- @return boolean whether the turtle succeeded in attacking downwards
function ProxyTurtle:attackDown(toolSide)
  if(self.pt_turtleFuncTable.pt_attackDown == nil) then
    return turtle.attackDown(toolSide)
  else
    return self.pt_turtleFuncTable.pt_attackDown(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Attempts to dig the block in front of the turtle.
-- If successful, suck() is automatically called, placing the item in turtle inventory in the selected slot if possible 
-- (block type matches and the slot is not a full stack yet), or in the next available slot.
-- If a hoe is used to attempt to "dig" a dirt block, it will be tilled instead.
-- Tilling is also possible if the space in front of the turtle is empty but dirt exists below that point.
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to dig with, 
-- otherwise the turtle will attempt to dig with both tools.
-- @return boolean whether the turtle succeeded in digging, string error message
function ProxyTurtle:dig(toolSide)
  if(self.pt_turtleFuncTable.pt_dig == nil) then
    return turtle.dig(toolSide)
  else
    return self.pt_turtleFuncTable.pt_dig(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Attempts to dig the block in front of the turtle.
-- If successful, suck() is automatically called, placing the item in turtle inventory in the selected slot if possible 
-- (block type matches and the slot is not a full stack yet), or in the next available slot.
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to dig with, 
-- otherwise the turtle will attempt to dig with both tools.
-- @return boolean whether the turtle succeeded in digging, string error message
function ProxyTurtle:digUp(toolSide)
  if(self.pt_turtleFuncTable.pt_digUp == nil) then
    return turtle.digUp(toolSide)
  else
    return self.pt_turtleFuncTable.pt_digUp(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Attempts to dig the block in front of the turtle.
-- If successful, suck() is automatically called, placing the item in turtle inventory in the selected slot if possible 
-- (block type matches and the slot is not a full stack yet), or in the next available slot.
-- When using a hoe, if the space beneath the turtle consists of air and the space beneath that is dirt, then the turtle may till that dirt by use of this function.
-- @param toolSide You can pass optionally pass in "left" or "right" to the toolSide argument to determine which tool to dig with, 
-- otherwise the turtle will attempt to dig with both tools.
-- @return boolean whether the turtle succeeded in digging, string error message
function ProxyTurtle:digDown(toolSide)
  if(self.pt_turtleFuncTable.pt_digDown == nil) then
    return turtle.digDown(toolSide)
  else
    return self.pt_turtleFuncTable.pt_digDown(toolSide)
  end
end

---------------------------------------------------------------------------------

--- Places the selected block in front of the Turtle
-- @param signText If you're placing a sign and signText is given, then the turtle places the sign putting the text of signText into it. 
-- Each line of the sign can be separated using newline ("\n") character.
-- @return boolean was the block placed?
function ProxyTurtle:place(signText)
  if(self.pt_turtleFuncTable.pt_place == nil) then
    return turtle.place(signText)
  else
    return self.pt_turtleFuncTable.pt_place(signText)
  end
end

---------------------------------------------------------------------------------

--- Places the selected block in front of the Turtle
-- @return boolean whether the turtle succeeded in placing the block. False if selected slot is empty, or if there is already a block above.
function ProxyTurtle:placeUp()
  if(self.pt_turtleFuncTable.pt_placeUp == nil) then
    return turtle.placeUp()
  else
    return self.pt_turtleFuncTable.pt_placeUp()
  end
end

---------------------------------------------------------------------------------

--- Places below a Block of the selected Slot.
-- @return boolean whether the turtle succeeded in placing the block. False if selected slot is empty, or if there is already a block below.
function ProxyTurtle:placeDown()
  if(self.pt_turtleFuncTable.pt_placeDown == nil) then
    return turtle.placeDown()
  else
    return self.pt_turtleFuncTable.pt_placeDown()
  end
end

---------------------------------------------------------------------------------

--- Detects if there is a Block in front. Does not detect mobs or liquids or floating items.
-- @return boolean If turtle has Detected a Block in front.
function ProxyTurtle:detect()
  if(self.pt_turtleFuncTable.pt_detect == nil) then
    return turtle.detect()
  else
    return self.pt_turtleFuncTable.pt_detect()
  end
end

---------------------------------------------------------------------------------

--- Detects if there is a block above the turtle.
-- @return boolean If turtle has Detected a Block in front.
function ProxyTurtle:detectUp()
  if(self.pt_turtleFuncTable.pt_detectUp == nil) then
    return turtle.detectUp()
  else
    return self.pt_turtleFuncTable.pt_detectUp()
  end
end

---------------------------------------------------------------------------------

--- Detects if there is a Block Below.
-- @return boolean If turtle has detected a block below.
function ProxyTurtle:detectDown()
  if(self.pt_turtleFuncTable.pt_detectDown == nil) then
    return turtle.detectDown()
  else
    return self.pt_turtleFuncTable.pt_detectDown()
  end
end

---------------------------------------------------------------------------------

--- Returns the ID string and metadata of the block in front of the Turtle in a table format: 
-- { name = "modname:blockname", metadata = 0, state = {} }.
-- The "state" sub-table is only present in Minecraft 1.8+, and contains block state info for the block being inspected. 
-- For example, a log of wood's state table contains the keys "variant" (which may be set to eg "oak"), and 
-- "axis" (which may be set to eg "y", indicating an upright orientation).
-- @return boolean success, table data/string error message
function ProxyTurtle:inspect()
  if(self.pt_turtleFuncTable.pt_inspect == nil) then
    return turtle.inspect()
  else
    return self.pt_turtleFuncTable.pt_inspect()
  end
end

---------------------------------------------------------------------------------

--- Returns the ID string and metadata of the block in front of the Turtle in a table format: 
-- { name = "modname:blockname", metadata = 0, state = {} }.
-- @return boolean success, table data/string error message
function ProxyTurtle:inspectUp()
  if(self.pt_turtleFuncTable.pt_inspectUp == nil) then
    return turtle.inspectUp()
  else
    return self.pt_turtleFuncTable.pt_inspectUp()
  end
end

---------------------------------------------------------------------------------

--- Returns the ID string and metadata of the block in front of the Turtle in a table format: 
-- { name = "modname:blockname", metadata = 0, state = {} }.
-- @return boolean success, table data/string error message
function ProxyTurtle:inspectDown()
  if(self.pt_turtleFuncTable.pt_inspectDown == nil) then
    return turtle.inspectDown()
  else
    return self.pt_turtleFuncTable.pt_inspectDown()
  end
end

---------------------------------------------------------------------------------

--- Detects if the block in front is the same as the one in the selected Slot.
-- @return boolean if the block in front is the same as the one in the selected Slot.
function ProxyTurtle:compare()
  if(self.pt_turtleFuncTable.pt_compare == nil) then
    return turtle.compare()
  else
    return self.pt_turtleFuncTable.pt_compare()
  end
end

---------------------------------------------------------------------------------

--- Detects if the block in above is the same as the one in the selected Slot.
-- @return boolean returns true if the block above the turtle is the same as the one in the selected slot, false otherwise
function ProxyTurtle:compareUp()
  if(self.pt_turtleFuncTable.pt_compareUp == nil) then
    return turtle.compareUp()
  else
    return self.pt_turtleFuncTable.pt_compareUp()
  end
end

---------------------------------------------------------------------------------

--- Detects if the block in below is the same as the one in the selected Slot.
-- @return boolean is the block below the turtle same as the one in the selected slot?
function ProxyTurtle:compareDown()
  if(self.pt_turtleFuncTable.pt_compareDown == nil) then
    return turtle.compareDown()
  else
    return self.pt_turtleFuncTable.pt_compareDown()
  end
end

---------------------------------------------------------------------------------

--- Detects if the block in the specified slot is the same as the one in the selected Slot.
-- @return boolean true if the selected item matches the one in the specified slot.
function ProxyTurtle:compareTo()
  if(self.pt_turtleFuncTable.pt_compareTo == nil) then
    return turtle.compareTo()
  else
    return self.pt_turtleFuncTable.pt_compareTo()
  end
end

---------------------------------------------------------------------------------

--- Drops all items off the selected slot, or, if count is specified, drops that many items. 
-- The items are dropped on the ground by default, but if there is an inventory in that block, the items go to that inventory instead. 
-- Then the items will be placed in the first available slot of the inventory, starting at the top left, moving right and then down.
-- @param count The amount you want to drop.
-- @return boolean true if an item was dropped; false otherwise.
function ProxyTurtle:drop(count)
  if(self.pt_turtleFuncTable.pt_drop == nil) then
    return turtle.drop(count)
  else
    return self.pt_turtleFuncTable.pt_drop(count)
  end
end

---------------------------------------------------------------------------------

--- Drops all items off the selected slot, or, if count is specified, drops that many items. 
-- The items are dropped on the space above the turtle by default, but if there is an inventory in that block, the items go to that inventory instead. 
-- Then the items will be placed in the first available slot of the inventory, starting at the top left, moving right and then down. 
-- Before Minecraft 1.5/ComputerCraft 1.51, dropUp would load the bottom (fuel) slot for furnaces, since the hopper-related changes, 
-- fuel is loaded into the sides, and the output slot is available on the bottom (using turtle.suckUp)
-- @param count The amount you want to drop.
-- @return boolean true if an item was dropped; false otherwise.
function ProxyTurtle:dropUp(count)
  if(self.pt_turtleFuncTable.pt_dropUp == nil) then
    return turtle.dropUp(count)
  else
    return self.pt_turtleFuncTable.pt_dropUp(count)
  end
end

---------------------------------------------------------------------------------

--- Drops all items off the selected slot, or, if count is specified, drops that many items. 
-- The items are dropped on the ground below the turtle by default, but if there is an inventory in that block, the items go to that inventory instead.
-- Then the items will be placed in the first available slot of the inventory, starting at the top left, moving right and then down. 
-- This command, when the turtle is above the furnace, will load the top slot for smelting.
-- @param count The amount you want to drop.
-- @return boolean true if an item was dropped; false otherwise.
function ProxyTurtle:dropDown(count)
  if(self.pt_turtleFuncTable.pt_dropDown == nil) then
    return turtle.dropDown(count)
  else
    return self.pt_turtleFuncTable.pt_dropDown(count)
  end
end

---------------------------------------------------------------------------------

--- Moves one or more items from either the ground in front of the turtle, or, from an inventory-enabled block (such as a chest) in front of the turtle.
-- > If an item is in the square directly in front of the turtle, it picks up one of those items.
-- > If a chest is in the square directly in front of the turtle, it picks up the items from the first non-empty chest slot, 
-- moving from top left to bottom right. The items are moved into the currently selected turtle slot if there is room.
-- > If the currently selected turtle slot is filled up before all of the items are picked up, the remaining picked up items are put in the next available turtle slot.
-- > If the currently selected turtle slot is 16 and the next slot is required, it will loop around and try turtle slot 1, and so on.
-- > As of ComputerCraft 1.6, if an amount is specified, the turtle will attempt to pick up at most the specified number of items. 
-- Earlier builds always attempt to pick up a full slot.
-- @param amount The amount you want to pick up.
-- @return boolean true if at least one item was moved into the turtle's inventory; false otherwise.
function ProxyTurtle:suck(amount)
  if(self.pt_turtleFuncTable.pt_suck == nil) then
    return turtle.suck(amount)
  else
    return self.pt_turtleFuncTable.pt_suck(amount)
  end
end

---------------------------------------------------------------------------------

--- Moves one or more items from either the ground above the turtle, or, from an inventory-enabled block (such as a chest) above the turtle.
-- > If an item is in the square directly above of the turtle, it picks up one of those items.
-- > If a chest is in the square directly above the turtle, it picks up the items from the first non-empty chest slot, moving from top left to bottom right. 
-- The items are moved into the currently selected turtle slot if there is room.
-- > If the currently selected turtle slot is filled up before all of the items are picked up, the remaining picked up items are put in the next available turtle slot.
-- > If the currently selected turtle slot is 16 and the next slot is required, it will loop around and try turtle slot 1, and so on.
-- > As of Minecraft 1.5/ComputerCraft 1.51, turtle.suckUp can be used to pick items of the output slot of a furnace.
-- > As of ComputerCraft 1.6, if an amount is specified, the turtle will attempt to pick up at most the specified number of items. 
-- Earlier builds always attempt to pick up a full slot.
-- @param amount The amount you want to pick up.
-- @return boolean true if at least one item was moved into the turtle's inventory; false otherwise.
function ProxyTurtle:suckUp(amount)
  if(self.pt_turtleFuncTable.pt_suckUp == nil) then
    return turtle.suckUp(amount)
  else
    return self.pt_turtleFuncTable.pt_suckUp(amount)
  end
end

---------------------------------------------------------------------------------

--- Moves one or more items from either the ground below the turtle, or, from an inventory-enabled block (such as a chest) below the turtle.
-- > If an item is in the square directly below the turtle, it picks up one of those items.
-- > If a chest is in the square directly below of the turtle, it picks up the items from the first non-empty chest slot, 
-- moving from top left to bottom right. The items are moved into the currently selected turtle slot if there is room.
-- > If the currently selected turtle slot is filled up before all of the items are picked up, the remaining picked up items are put in the next available turtle slot.
-- > If the currently selected turtle slot is 16 and the next slot is required, it will loop around and try turtle slot 1, and so on.
-- > As of ComputerCraft 1.6, if an amount is specified, the turtle will attempt to pick up at most the specified number of items. 
-- Earlier builds always attempt to pick up a full slot.
-- @param amount The amount you want to pick up.
-- @return boolean true if at least one item was moved into the turtle's inventory; false otherwise.
function ProxyTurtle:suckDown(amount)
  if(self.pt_turtleFuncTable.pt_suckDown == nil) then
    return turtle.suckDown(amount)
  else
    return self.pt_turtleFuncTable.pt_suckDown(amount)
  end
end

---------------------------------------------------------------------------------

--- If the currently selected slot contains fuel items, it will consume them to give the turtle the ability to move. 
-- If the current slot doesn't contain fuel items, it returns false. If a quantity is specified, it will refuel only up to that many items. 
-- If the avaiable items quantity is lower than the specified, only the avaiable quantity will be consumed, because it can't consume what doesn't exist. 
-- If no quantity is specified, it will consume all the items in the slot. Turtles loose all the fuel when broken if they don't have a label.
-- > As of ComputerCraft 1.6, turtles may no longer store practically unlimited amounts of fuel. 
-- Attempting to refuel past a given turtle's limit with this command will destroy the items regardless, 
-- but fails to raise the fuel level higher than maximum and returns no error
-- @param quantity The quantity you want to use to refuel.
-- @return 	boolean true if fueled, else false.
function ProxyTurtle:refuel(quantity)
  if(self.pt_turtleFuncTable.pt_refuel == nil) then
    return turtle.refuel(quantity)
  else
    return self.pt_turtleFuncTable.pt_refuel(quantity)
  end
end

---------------------------------------------------------------------------------

--- Returns how much fuel is inside the specific turtle. 
-- Turtles may be configured to ignore the need for fuel via ComputerCraft.cfg, in which case this command will always return "unlimited". 
-- Turtles loose their fuel when destroyed, if they don't have a label.
-- @return "unlimited" if fuel is disabled, otherwise the fuel level.
function ProxyTurtle:getFuelLevel()
  if(self.pt_turtleFuncTable.pt_getFuelLevel == nil) then
    return turtle.getFuelLevel()
  else
    return self.pt_turtleFuncTable.pt_getFuelLevel()
  end
end

---------------------------------------------------------------------------------

--- Added by version 1.6 of ComputerCraft, this command returns the maximum amount of fuel a turtle may store. 
-- By default, a regular turtle may hold 20,000 units, and an advanced model 100,000 units; both values can be changed in ComputerCraft.cfg.
-- > In builds prior to 1.6, turtles may store practically unlimited amounts of fuel.
-- @return "unlimited" if fuel is disabled, otherwise the maximum fuel level.
function ProxyTurtle:getFuelLimit()
  if(self.pt_turtleFuncTable.pt_getFuelLimit == nil) then
    return turtle.getFuelLimit()
  else
    return self.pt_turtleFuncTable.pt_getFuelLimit()
  end
end

---------------------------------------------------------------------------------

--- Transfers quantity items from the selected slot to the specified slot. 
-- If the quantity argument is omitted, tries to transfer all the items from the selected slot. 
-- If the destination slot already has items of a different type, returns false (does not try to fill the next slot, like suck() would). 
-- If there are fewer than quantity items in the selected slot or only room for fewer items in the destination slot, transfers as many as possible and returns true. 
-- If none can be transferred, returns false.
-- @param slot The slot where items shall be transfered to.
-- @param quantity (Optional) The quantity which shall be transfered.
-- @return 	boolean success
function ProxyTurtle:transferTo(slot,quantity)
  if(self.pt_turtleFuncTable.pt_transferTo == nil) then
    return turtle.transferTo()
  else
    return self.pt_turtleFuncTable.pt_transferTo()
  end
end

---------------------------------------------------------------------------------
--[[

local pTurtle = ProxyTurtle:new()
pTurtle:forward()
pTurtle:back()
pTurtle:turnLeft()
pTurtle:turnRight()
pTurtle:turnAround()
pTurtle:up()
pTurtle:down()
pTurtle:digDown()

--]]
