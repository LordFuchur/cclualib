#!/usr/bin/lua5.1

function stair()
  turtle.digDown()
  -- clear 3 (4) blocks above
  for i = 1, 3 do
    if(turtle.detectUp()) then
      turtle.digUp()
      turtle.up()
    else
      turtle.up()
    end
  end
  -- move down
  for i = 1, 5 do
      turtle.down()
  end
  if(not turtle.detectDown()) then
    turtle.placeDown()
  end
  turtle.dig()
  turtle.forward()
end

stair()