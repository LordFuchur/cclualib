#!/usr/bin/lua5.1

--- Print header of this application
function header()
  print("------------------------------")
  print("--------> Stationfarm <-------")
  print("------------------------------\n")
end

--- Find a block of a specific type
-- Id strings can be found here: 
-- https://minecraft-ids.grahamedgecombe.com/
-- @param id Minecraft block id string like "modname:itemname"
function findBlockOfType(id)
  --- search gravel and select slot
  local data
  -- do a quick search on the current slot
  local curSlot = turtle.getSelectedSlot()
  data = turtle.getItemDetail()
  if(data ~= nil and data.name == id) then
    return true, turtle.getSelectedSlot()
  end
  
  for slot = 1, 16 do
    turtle.select(slot)
    -- get item details { name = "modname:itemname", damage = 0, count = 1 }
    data = turtle.getItemDetail()
    -- check block type inside the slot
    if(data ~= nil and data.name == id) then
        return true, turtle.getSelectedSlot()
    end
  end
  return false, turtle.getSelectedSlot()
end

--- Turtle function to dig an amount of cobblestone
-- @param amount The amount of cobblestone you want to farm
function cobblestone(amount)
  local counter = 0
  -- do work
  print("Start working task of " .. amount .. " cobblestones")
  while(counter < amount) do
    -- get block details infront of the turtle
    -- { name = "modname:blockname", metadata = 0, state = {} }
    local success, data = turtle.inspect()
    if(success and data.name == "minecraft:cobblestone") then
      turtle.dig()
      counter = counter + 1
    elseif(success) then
      print("Ileagal block in front detected ! Please remove it !!")
    end
  end
  -- drop all cobblestone down (inside a hopper or a chest)
  local foundSlot,slotNum = findBlockOfType("minecraft:cobblestone")
  while(foundSlot) do
      turtle.select(slotNum) -- shouldnt be nessesary but to be sure
      turtle.dropDown()
       -- re-search slots
      foundSlot,slotNum = findBlockOfType("minecraft:cobblestone")
  end  
  print("Task of " .. amount .. " cobblestones complete")
end

--- Turtle function to process gravel into flint
function flint()
  print("Start working task flint")
  local foundSlot,slotNum = findBlockOfType("minecraft:gravel")
  --- do work so long gravel exist in the turtle inventory
  while(foundSlot) do
    -- check enough gravel
    turtle.select(slotNum) -- shouldnt be nessesary but to be sure
    while(turtle.getItemCount(slotNum) > 0) do
      turtle.place()
      turtle.dig()
      -- re-search slots
      foundSlot,slotNum = findBlockOfType("minecraft:gravel")
    end
  end
  print("Gravel is empty !")
  -- drop all flints down (inside a hopper or a chest)
  foundSlot,slotNum = findBlockOfType("minecraft:flint")
  while(foundSlot) do
      turtle.select(slotNum) -- shouldnt be nessesary but to be sure
      turtle.dropDown()
       -- re-search slots
      foundSlot,slotNum = findBlockOfType("minecraft:flint")
  end
end

--- Menu function for the stationfarm module
function menu()
  local selection = nil
  local amount = 0
  
  -- menu loop
  repeat
      print("Please select a program\n")
      print("0) Close Repository Installer")
      print("1) Flint")
      print("2) Cobblestone")
      selection = io.read("*l")
      selection = tonumber(selection)
      assert(type(selection) == "number")
      -- check how much cobblestone when selected
      if(tonumber(selection) == 2) then
          print("Please enter how much cobblestone:")
          amount = io.read("*l")
          amount = tonumber(amount)
          assert(type(amount) == "number")
      end
  until( selection ~= nil)
  -- check to cancel
  if(tonumber(selection) == 0) then
      shell.exit()
    end  
  -- return selection
  return tonumber(selection), tonumber(amount)
end


---------------------------------------------------------------------
-- Execute Code
---------------------------------------------------------------------
header()
local choice, amount = menu()

if(choice == 1) then
  flint()
elseif(choice == 2) then
  cobblestone(amount)
elseif(choice == 0) then
  print("Closing...")
else
  print("ERROR: Something went wrong.")
end