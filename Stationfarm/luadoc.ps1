$files = Get-ChildItem .\ *.lua -Recurse
$aFiles = @()
foreach( $file in $files ) 
{
    $aFiles += $file.Name
}
$aFiles += "-d"
$aFiles += "./doc"
&luadoc_start.bat $aFiles